package com.example.rafalsuski.p2promo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private long backPressedTime;



      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
          if (mToggle.onOptionsItemSelected(item)){
              return true;
          }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.menubar, menu);
        return true;

    }

    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        }

        else {
            Toast.makeText(getBaseContext(), "Press back again to exit", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    public void clickCamera(MenuItem item) {

        if (item.getItemId()==R.id.camera){
            Intent i = new Intent(MainActivity.this, MainCamera.class);
            startActivity(i);
            //finish();
        }
    }

    public void clickHelp(MenuItem item) {
        if (item.getItemId()==R.id.help){
            Intent i = new Intent(MainActivity.this, HelpActivity.class);
            startActivity(i);

        }
    }

    public void clickExit(MenuItem item) {
        if (item.getItemId() == R.id.exitSystem) {
            Intent i = new Intent(MainActivity.this, MainExit.class);
            startActivity(i);
            finish();
        }
    }


    //public void clickHelp(View view){

        //new PreferenceManager(this).clearPreference();
       // startActivity(new Intent(this, HelpActivity.class));
       // finish();
    //}

}




